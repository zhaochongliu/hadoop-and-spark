1. For the whole file, I always get the out of memory error. For this question, I use the file of "part-00000" to "part-00999", in total 1000 files. The number of unique words I got is 562,102
2. For this question, the first 1000 files is still too big to be executed... I use the 100 files start with "part-001". 
  topic1: youdesirecindy
  topic2: oneillinstitute
  topic3: michaelxanthony
  topic4: kajerstinpinc
  topic5: usmhuskies
  topic6: poppykicker
  topic7: wineenthusiast
  topic8: mature
  topic9: masturbat
  topic10:httptcomieydwiura
3. The words in the file named stopwords.txt is the file I choosed to be stopwords.
4. topic1: whored
   topic2: marieantoinette
   topic3: fallingdrowningand
   topic4: iam_vaniquee
   topic5: httptcosjktr5sxw5
   topic6: yo_g_lowkey
   topic7: no__parachutes
   topic8: httptcovyhyamyidf
   topic9: jojo_isola
   topic10:discernment
5. First, we need to get the document i's topic distribution from the model. From this dictribution, we can 
get the topic of each words from the document. After this, for each topic we get, we can use the distribution learned from the LDA model to get the word distribution. From the word distribution, we can finally get the word of the document. By doing this, we can classify this document by analysing the words we generated from the document.
