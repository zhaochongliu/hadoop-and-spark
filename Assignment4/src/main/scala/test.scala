import org.apache.spark._
import org.apache.spark.mllib.clustering.LDA
import org.apache.spark.mllib.linalg._
import scala.io.Source
import java.io._

object Test{

	def arytowd(a: String): Array[String]  =  {
                val line = a.split("\\s+").drop(1)
		val data = line.map(word => word.replaceAll("\\W", "").toLowerCase)
		data
	}

	def main(args:Array[String]){
		val conf = new SparkConf().setAppName("Test")
		val sc   = new SparkContext(conf)
		val file = "/home/zcliu/projects/Assignment4/twitter-processed/part-001*"
		val stopwords = "/home/zcliu/projects/Assignment4/stopwords.txt"
                val stopword = sc.textFile(stopwords)
		val id = 0.0
		val tweetsfile = sc.textFile(file)

                val data = tweetsfile.flatMap(line => line.split("\\s+").drop(1))
                var wordlist = data.map(word => word.replaceAll("\\W", "").toLowerCase).distinct
                wordlist = wordlist.subtract(stopword)
		val wordid = wordlist.zipWithIndex
		val uniquewords = wordid.count.toInt
                println(uniquewords)
		//make documents to arrays of words and give documents label
		val linelist = tweetsfile.map(line => arytowd(line)).zipWithIndex

		//(single word, {document ids})
                val wordsdoc = linelist.flatMap(wd => wd._1.map(w => (w, wd._2))).groupByKey

                //(word, (id, {document ids}))
		val joinlist = wordsdoc.join(wordid)

                //(id of the word, {document ids})
		val worddocs = joinlist.map(x => x._2)

		//(document id, {word ids})		
		val docwords = worddocs.flatMap(dw => dw._1.map(d => (dw._2, d))).groupByKey

                //(document id, ({unique word ids}, {numbers of the words}))
		val parseddata = docwords.map(line => (line._1, line._2.groupBy(identity).mapValues(_.size).unzip))
		val docv = parseddata.map(line => (line._1, Vectors.sparse(uniquewords, line._2._1.toArray.map(x => x.toInt), line._2._2.toArray.map(x => x.toDouble))))
		val top = new PrintWriter(new File("/home/zcliu/projects/Assignment4/topics.txt"))
		val lda = new LDA().setK(10).run(docv)
		val topicIndices = lda.describeTopics(10)
		topicIndices.foreach { case (terms, termWeights) =>
			top.write("Topic: \n")
			sc.parallelize(terms.zip(termWeights)).join(wordid.map(w => (w._2.toInt, w._1))).map(x => x._2).collect().foreach { case (term, weight) =>
				top.write(term + "  " + weight + "\n")
			}
			
		}
		top.close
	}
}

