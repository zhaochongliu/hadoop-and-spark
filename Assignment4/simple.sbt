
name := "Test"

scalaVersion := "2.10.4"

javaOptions += "-Xmx6g"

libraryDependencies += "org.apache.commons" % "commons-math3" % "3.4.1"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0"

libraryDependencies += "org.apache.spark" % "spark-streaming_2.10" % "1.3.0"

libraryDependencies += "org.apache.spark" % "spark-mllib_2.10" % "1.3.0"
