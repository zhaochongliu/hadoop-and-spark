from HTMLParser import HTMLParser
import sre, urllib2, sys, BaseHTTPServer
import socket

class MyHTMLParser(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.start_line = -1
		self.end_line   = -1
		self.index      = 0
		self.p_index    = 0
		self.text       = ''
		self.title1     = -1
		self.title2     = -1
		self.title      = ''
		self.t_index    = 0
		self.title_line = -1
	def handle_starttag(self, tag, attrs):
		if(tag == 'div'):
			if(attrs == [('id','tn15content')]):
                        	self.start_line = self.getpos()[0]
			if(attrs == [('id', 'tn15title')]):
				self.title1 = self.getpos()[0]
			if(self.start_line > -1):
				self.index += 1
			if(self.title1 > -1):
				self.t_index += 1
		if(self.title1 > -1 and self.title2 == -1 and tag == 'a' and attrs[0] == ('class', 'main')):
			self.title_line = self.getpos()[0]
		else:
			 self.title_line = -1
		if(self.start_line > -1 and self.end_line == -1):
			if(tag == 'img' and attrs[2][0] == 'alt'):
				self.text = attrs[2][1] + '\t'
			if(tag == 'p'):
				self.p_index = 1

	def handle_endtag(self, tag):
		result = 'traindata.txt'
#		result = '../' + self.title + '.txt'
		textfile = open(result, 'a')
		if(tag == "div" and self.start_line > -1):
			self.index -= 1
			if(self.index == 0):
				self.end_line = self.getpos()[0]
		if(tag == "div" and self.title1 > -1):
			self.t_index -= 1
			if(self.t_index == 0):
				self.title2 = self.getpos()[0]
		if(self.start_line > -1 and self.end_line == -1):
			if(tag == 'p'):
				self.p_index = 0
				if(len(self.text) > 7):
					textfile.write(self.text + '|||||' + self.title + '\n')
					self.text = ''
					return
		textfile.close()

	def handle_data(self, data):
		if(self.p_index == 1):
			self.text = self.text + data.replace('\n',' ').replace('*** This review may contain spoilers ***', '').replace('Add another review','')
		if(self.title_line > -1 and data != ' '):
			self.title = data.replace('\n', '').replace('[^0-9a-zA-Z]+', '')
			print self.title

def parseAddress(input):
        if input[:7] != "http://":
                if input.find("://") != -1:
                        print "Error: Cannot retrieve URL, protocol must be HTTP"
                        sys.exit(1)
                else:
                        input = "http://" + input
        return input

def retrieveWebPage(address):
        try:
                web_handle = urllib2.urlopen(address, timeout = 10)
        except urllib2.HTTPError, e:
                error_desc = BaseHTTPServer.BaseHTTPRequestHandler.responses[e.code][0]
                print "Cannot retrieve URL: HTTP Error Code", e.code
                web_handle =  None
        except urllib2.URLError, e:
                print "Cannot retrieve URL: " 
		web_handle =  None
#                sys.exit(1)
	except socket.timeout, e:
		print "time out: "  
		web_handle = None
        except:
                print "Cannot retrieve URL: unknown error"
		web_handle = None
        return web_handle
	
def readpage(website_handle):
	text = None
	if (website_handle.__class__.__name__ != 'NoneType'):
		try:
			text = website_handle.read()
		except socket.timeout, e:
			print "time out: " 
			text = None
	return text
#if len(sys.argv) < 2:
#        print "Usage:"
#        print "%s url" % (sys.argv[0])
#        sys.exit(1)

#socket.setdefaulttimeout(10)

#with open('urls.txt', 'r') as f:
#	for line in f.readlines():
#		print line
#		address = parseAddress(line)
#		website_handle = retrieveWebPage(address)
#		website_text = readpage(website_handle)
#		if (website_text != None):
#			parser = MyHTMLParser()
#			parser.feed(website_text)
#			
#			match = sre.findall('.*reviews in total', website_text)
#			print match
#			if (len(match) != 0):
#				num = match[0].split(' ')[0]
#				x = int(num)/10
#				page = set()
#				pagestring = 'start='
#
#				while(x > 0):
#
#					start_lable = x * 10
#					page_string = line.replace('\n', '') + pagestring + str(start_lable)
#					page.add(page_string)
#					x -= 1
#
#				for url in page:
#					print url
#					address = parseAddress(url)
#					website_handle = retrieveWebPage(address)
#					website_text = readpage(website_handle)
#					if (website_text != None):
#						parser = MyHTMLParser()
#						parser.feed(website_text)
#
