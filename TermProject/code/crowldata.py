from pyspark import SparkContext
from serial import parseAddress
from serial import MyHTMLParser
from serial import retrieveWebPage
from serial import readpage
from HTMLParser import HTMLParser
import sre, urllib2, sys, BaseHTTPServer

def crowl(adstring):
	print adstring
	address = parseAddress(adstring)
	website_handle = retrieveWebPage(address)
	website_text = readpage(website_handle)
	if (website_text != None):
		parser = MyHTMLParser()
	        parser.feed(website_text)
	        match = sre.findall('.*reviews in total', website_text)

	        if (len(match) != 0):
			num = match[0].split(' ')[0]
			x = int(num)/10
		        page = set()
		        pagestring = 'start='
		        while(x > 0):
		                start_lable = x * 10
	        	        page_string = adstring.replace('\n', '') + pagestring + str(start_lable)
	        	        page.add(page_string)
	              		x -= 1

	        	for url in page:
	                	address = parseAddress(url)
	                	website_handle = retrieveWebPage(address)
                		website_text = readpage(website_handle)
				if (website_text != None):

        	        		parser = MyHTMLParser()
	                		parser.feed(website_text)

logFile = "/home/zcliu/6900/TermProject/code/url.txt"

sc = SparkContext('local', 'Simple App')

logData = sc.textFile(logFile).map(lambda line: crowl(line)).collect()



