from HTMLParser import HTMLParser
import sre, urllib2, sys, BaseHTTPServer


class MyHTMLParser(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.start_line = -1
		self.end_line   = -1
		self.index      = 0
		self.p_index    = 0

	def handle_starttag(self, tag, attrs):
		textfile = open('newfile.txt', 'a')
		if(tag == 'div'):
			if(attrs == [('id','tn15content')]):
                        	self.start_line = self.getpos()[0]
			if(self.start_line > -1):
				self.index += 1
		if(self.start_line > -1 and self.end_line == -1):
			if(tag == 'img' and attrs[2][0] == 'alt'):
				textfile.write('\n' + attrs[2][1] + '\t')
			if(tag == 'p'):
				self.p_index = 1
		textfile.close()

	def handle_endtag(self, tag):
		if(tag == "div" and self.start_line > -1):
			self.index -= 1
			if(self.index == 0):
				self.end_line = self.getpos()[0]
		if(self.start_line > -1 and self.end_line == -1):
			if(tag == 'p'):
				self.p_index = 0

	def handle_data(self, data):
		textfile = open('newfile.txt', 'a')
		if(self.p_index == 1):
			textfile.write(data)
		textfile.close()

def parseAddress(input):
        if input[:7] != "http://":
                if input.find("://") != -1:
                        print "Error: Cannot retrieve URL, protocol must be HTTP"
                        sys.exit(1)
                else:
                        input = "http://" + input
        return input


def retrieveWebPage(address):
        try:
                web_handle = urllib2.urlopen(address)
        except urllib2.HTTPError, e:
                error_desc = BaseHTTPServer.BaseHTTPRequestHandler.responses[e.code][0]
                #print "Cannot retrieve URL: " + str(e.code) + ": " + error_desc
                print "Cannot retrieve URL: HTTP Error Code", e.code
                sys.exit(1)
        except urllib2.URLError, e:
                print "Cannot retrieve URL: " + e.reason[1]
                sys.exit(1)
        except:
                print "Cannot retrieve URL: unknown error"
        return web_handle

if len(sys.argv) < 2:
        print "Usage:"
        print "%s url" % (sys.argv[0])
        sys.exit(1)

address = parseAddress(sys.argv[1])
website_handle = retrieveWebPage(address)
website_text = website_handle.read()

match = sre.findall('.*reviews in total', website_text)
num = match[0].split(' ')[0]
x = int(num)/10

page = set()
pagestring = 'start='
while(x > -1):
	start_lable = x * 10
	page_string = sys.argv[1] + pagestring + str(start_lable)
	page.add(page_string)
	x -= 1

for url in page:
	print url
	address = parseAddress(url)
	website_handle = retrieveWebPage(address)
	website_text = website_handle.read()
	
	parser = MyHTMLParser()
	parser.feed(website_text)


