import java.io.*;
import java.lang.*;
import java.util.*;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Counters;
public class KM {
  public static final int CLUSTER = 10;
  public static final double THRESH = 0.00000001;
  
  public static enum Move {
    COUNTER
  }
  public static class Instances{
    public int index;
    public double[] coordinates = new double[384];

    public Instances(String doc){
      StringTokenizer words = new StringTokenizer(doc,"\t",false);
      StringTokenizer label = new StringTokenizer(words.nextToken());
      StringTokenizer frame = new StringTokenizer(words.nextToken(),",",false);
      index = Integer.parseInt(label.nextToken());
      int i = 0;
      while(frame.hasMoreTokens()) {
        coordinates[i] = Double.parseDouble(frame.nextToken());
        i++;
      }
    }

    public double distance(double[] cluster){
      double sum = 0;
      double square;
      for(int i = 0; i < 384; i++) {
        square = (cluster[i] - coordinates[i]) * (cluster[i] - coordinates[i]);
        sum += square;
      }
      return sum;
    }
     
    public double[] summation (double[] value){
      double[] sum = new double[384];
      for(int i = 0; i < 384; i++) {
        sum[i] = value[i] + coordinates[i];
      }
      return sum;
    }
  } 
  public static class MapClass
       extends Mapper<Object, Text, Text, Text> {
    
    private int cluster;
    private int index;
    private Map<Integer,Double[]> dic = new HashMap<Integer,Double[]>();
 
    @Override
    public void setup(Context context
                     )throws IOException, InterruptedException {
      FileSystem sys;
      BufferedReader read;
      Path path;
      String line;
      for(URI CacheFile: context.getCacheFiles()) {
        path = new Path(CacheFile.toString());
        sys = FileSystem.get(path.getParent().toUri(),context.getConfiguration());
        read = new BufferedReader(new InputStreamReader(sys.open(path)));
        line = read.readLine();
        while (line != null) {
          Instances inst = new Instances(line);
          cluster = inst.index;
          dic.put(cluster,dtD(inst.coordinates));
          line = read.readLine();
        }
      }
    }
    
    public Double[] dtD(double[] arr) {
      Double[] Arr = new Double[384];
      for(int i = 0; i < 384; i++) {
        Arr[i] = new Double(arr[i]);
      }
      return Arr;
    }
    public double[] Dtd(Double[] Arr) {
      double[] arr = new double[384];
      for(int i = 0; i < 384; i++) {
        arr[i] = Arr[i].doubleValue();
      }
      return arr;
    }

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      Instances inst = new Instances(value.toString());
     
      cluster = 0;
      index = inst.index;
      double[] coo = inst.coordinates;

      double closest = inst.distance(Dtd(dic.get(cluster)));
      double dist;
      for(int i = 1; i < CLUSTER; i++){
        dist = inst.distance(Dtd(dic.get(i)));
        if(dist < closest){
          cluster = i;
          closest = dist;
        }
      }

      context.write(new Text( cluster + ""), new Text(value.toString() + ":1"));
    }
  }

  public static class Combine
       extends Reducer<Text, Text, Text, Text> {
   
    public void reduce(Text key, Iterable<Text> values, Context context
                      ) throws IOException, InterruptedException {
      double[] sum = new double[384];
      int num = 0;
      for(Text value : values) {
        String[] images = value.toString().split(":");
        Instances inst = new Instances(images[0]);
        sum = inst.summation(sum);
        num ++;
      }
      
      String output = num + "\t";
      for(double element : sum){
        output += (element + ",");  
      }

      context.write(key,new Text(output)); 
    }    
  }
  public static class Reduce
       extends Reducer<Text,Text,Text,Text> {
    private int cluster;
    private int index;
    private Map<Integer,Double[]> dic = new HashMap<Integer,Double[]>();

    @Override
    public void setup(Context context
                     )throws IOException, InterruptedException {
      FileSystem sys;
      BufferedReader read;
      Path path;
      String line;
      for(URI CacheFile: context.getCacheFiles()) {
        path = new Path(CacheFile.toString());
        sys = FileSystem.get(path.getParent().toUri(),context.getConfiguration());
        read = new BufferedReader(new InputStreamReader(sys.open(path)));
        line = read.readLine();
        while (line != null) {
          Instances inst = new Instances(line);
          cluster = inst.index;
          dic.put(cluster,dtD(inst.coordinates));
          line = read.readLine();
        }
      }
    }

    public Double[] dtD(double[] arr) {
      Double[] Arr = new Double[384];
      for(int i = 0; i < 384; i++) {
        Arr[i] = new Double(arr[i]);
      }
      return Arr;
    }
    public double[] Dtd(Double[] Arr) {
      double[] arr = new double[384];
      for(int i = 0; i < 384; i++) {
        arr[i] = Arr[i].doubleValue();
      }
      return arr;
    }


    public void reduce(Text key, Iterable<Text> values,
                       Context context
                       ) throws IOException, InterruptedException {
      double[] sum = new double[384];
      int num = 0;
      for(Text value : values) {
        Instances inst = new Instances(value.toString());
        num += inst.index;
        sum = inst.summation(sum);
      }
      for(int i = 0; i < 384; i++) {
        sum[i] = sum[i]/num;        
      }      
      String output = ",";
      for(double element : sum) {
        output += (element + ",");
      }

      double x = 0;
      int index = Integer.parseInt(key.toString());
      for(int i = 0; i < 384; i++) {
        x = sum[i] - Dtd(dic.get(index))[i];
        if(x > THRESH) {
          context.getCounter(Move.COUNTER).increment(1);
        }
      }
      context.write(key, new Text(output));

      
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "k-means");
    job.addCacheFile(new URI("/user/zcliu/cache/centroids10.small.txt"));
    job.setJarByClass(KM.class);
    job.setMapperClass(MapClass.class);
    job.setCombinerClass(Combine.class);
    job.setReducerClass(Reduce.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path("/user/zcliu/output/iter_1"));
    job.waitForCompletion(true);

    int iteration = 1;
    Counters counters = job.getCounters();
    Counter counter = counters.findCounter(Move.COUNTER);
    long c = counter.getValue();
    long delta = 0;
    String source = "/user/zcliu/output/iter_"+iteration;
    
    while(c > delta) {

      iteration++;

      conf = new Configuration();
      FileSystem fs = FileSystem.get(new URI(source),conf);
      job = Job.getInstance(conf, "k-means");
      FileStatus[] names = fs.listStatus(new Path(source));

      for(int i = 0; i < names.length; i++) {
         job.addCacheFile(names[i].getPath().toUri());
      }

      job.setJarByClass(KM.class);
      job.setMapperClass(MapClass.class);
      job.setCombinerClass(Combine.class);
      job.setReducerClass(Reduce.class);
      job.setOutputKeyClass(Text.class);
      job.setOutputValueClass(Text.class);
      FileInputFormat.addInputPath(job, new Path(args[0]));
      source = "/user/zcliu/output/iter_"+iteration;
      FileOutputFormat.setOutputPath(job, new Path(source));
      job.waitForCompletion(true);  
      
      counters = job.getCounters();
      counter = counters.findCounter(Move.COUNTER);
      c = counter.getValue();
         
    }
  }
}
