import java.io._
import javax.imageio._
import org.apache.commons.math3.linear._
import org.apache.spark._
import java.awt.image.BufferedImage

object Test
{
    def rawImg(img : File) : RealMatrix = {
        val image = ImageIO.read(img)
        val cols = image.getWidth()
        val rows = image.getHeight()
        val data = MatrixUtils.createRealMatrix(rows, cols)
        var x:Array[Double] = new Array[Double](cols)
        for(i <- 0 until rows) {
            for(j <- 0 until cols){
                x(j) = image.getRGB(i,j)
            }
            data.setRow(i, x)
        }
        data
    }
    
    def Flatten(img : File) : Array[Double] = {
        val matrix = rawImg(img)
        val data = matrix.getData()
        val flatMatrix = data.flatten
        
        flatMatrix
    }
    
    def Reconstruct(img : File) : RealMatrix = {
        val data = rawImg(img)
        val k = 50
        val temp = new SingularValueDecomposition(data)
        val u = temp.getU()
        val s = temp.getS()
        val vt = temp.getVT()
        
        val Ru = u.getSubMatrix(0,u.getRowDimension()-1,0,k-1)
        val Rs = s.getSubMatrix(0,k-1,0,k-1)
        val Rvt = vt.getSubMatrix(0,k-1,0,vt.getColumnDimension()-1)

        val RData = Ru.multiply(Rs.multiply(Rvt))
        RData
    }

    def getAvg(m : RealMatrix) : RealMatrix = {
        var o = 0.00
        for(i <- 0 until m.getRowDimension(); j <- 0 until m.getColumnDimension()) {
            o = m.getEntry(i,j)
            m.setEntry(i,j,o/1000)
        }
        m
    }

    def printResult(result : RealMatrix) {
        val w = result.getColumnDimension
        val h = result.getRowDimension
        var image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
        for(i <- 0 until w; j <- 0 until h) {
            image.setRGB(i, j, result.getEntry(j, i).toInt)
        }
        val output = new File("/home/zcliu/projects/Assignment3/output50.png")
        ImageIO.write(image, "png" , output)
    }
    
    def main(args:Array[String]){
        val conf = new SparkConf().setAppName("Test")
        val sc   = new SparkContext(conf)
        val file = new File("/home/zcliu/projects/test/")
 
        //val imgFiles = sc.parallelize(file.listFiles()).map(x => Reconstruct(x))
        //val sum = imgFiles.reduce(_.add(_))
        //val avg = getAvg(sum)
        //val output = sc.parallelize(avg.getData())
        //output.saveAsTextFile("/home/zcliu/project/Assignment3/output")
        //printResult(avg)



        val imgFiles = sc.parallelize(file.listFiles()).map(x => Flatten(x)).collect()
        
        val data = MatrixUtils.createRealMatrix(imgFiles.length, imgFiles(0).length)
        for( i <- 0 until imgFiles.length){
            data.setRow(i,imgFiles(i))
        }

        var pixel: Array[Double] = new Array[Double](imgFiles.length)
        val span: Array[Double] = new Array[Double](imgFiles(0).length)
        for(i <- 0 until imgFiles(0).length){
            pixel = data.getColumn(i)
            span(i) = pixel.max - pixel.min
        }
        val i = 0
     
        val sortedArray = span.sortWith(_>_)

        val indices: Array[Int] = new Array[Int](10)
        indices(0) = span.indexOf(sortedArray(0))
        print(indices(i) + ",")
        for(i <- 1 until 10){ 
            if (sortedArray(i) == sortedArray(i-1)){
                
                indices(i) = span.indexOf(sortedArray(i), indices(i-1)+1)
            }
            else indices(i) = span.indexOf(sortedArray(i))
            print(indices(i) + ",")
        }
        
    }
}
