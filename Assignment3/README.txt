1. --master local       1079.966022 s
   --master local[4]     782.037084 s
   the run time is different. This commond is the master url to spark. According to the document, the number is the number of cores to allocate for each task. So it should be faster when there are more cores. And I expect less time if I use multiple cores across multiple machines.

2. k = 10: 5.271170049776917E9
   k = 50: 3.3151057774301505E9
   k = 100: 2.1453228077618601E9
   with the k increasing, the run time increases and the norm of ||A - Ak|| decreases. There should be an ideal choice of k. 
We can set a threshold. Then calculate the difference between two norms. The norms come from ||A - Ak|| and ||A - A(k+1)||. If the difference is smaller than the threshold, then we get the ideal k.

3. It is always out of memory, so I just use first 50 images.  I flattened the matrix in rows and get the indices:173558,85863,121928,140219,88981,30773,5480,84985,111585,5691

4. We can get the indices of the top spans. Then just focus on these indices. Compare each pixel and to see if the two image can be send into the same bucket. Then, each pixel can take a vote. According to the vote of each pixel(or index), we can calculate the propobility of which bucket the image should fall into.
