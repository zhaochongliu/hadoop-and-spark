import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;
public class WordCount {

  public static enum WORD_COUNTERS{
    UNIQUE_WORDS,
    CCAT,
    CCAT_SUM,
    ECAT,
    ECAT_SUM,
    GCAT,
    GCAT_SUM,
    MCAT,
    MCAT_SUM,
    ALL_TRAIN
  };

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer labels = new StringTokenizer(value.toString(),"\t",false);
      StringTokenizer label  = new StringTokenizer(labels.nextToken(),",",false);
      StringTokenizer itr    = new StringTokenizer(labels.nextToken());
      String l;
      String WordOfD;
      String SumOfWords;
      context.getCounter(WORD_COUNTERS.ALL_TRAIN).increment(1);
      while (label.hasMoreTokens()) {
        l = label.nextToken();
        if (l.equals("CCAT")){
          context.getCounter(WORD_COUNTERS.CCAT).increment(1);
          while(itr.hasMoreTokens()) {
            context.getCounter(WORD_COUNTERS.CCAT_SUM).increment(1);
            WordOfD = l+"/"+itr.nextToken();
            word.set(WordOfD);
            context.write(word, one);
          }
        }
        else if (l.equals("ECAT")){
          context.getCounter(WORD_COUNTERS.ECAT).increment(1);
          while(itr.hasMoreTokens()) {
            context.getCounter(WORD_COUNTERS.ECAT_SUM).increment(1);
            WordOfD = l+"/"+itr.nextToken();
            word.set(WordOfD);
            context.write(word, one);
          }  
        }
        else if (l.equals("GCAT")){
          context.getCounter(WORD_COUNTERS.GCAT).increment(1);
          while(itr.hasMoreTokens()) {
            context.getCounter(WORD_COUNTERS.GCAT_SUM).increment(1);
            WordOfD = l+"/"+itr.nextToken();
            word.set(WordOfD);
            context.write(word, one);
          }
        }
        else if (l.equals("MCAT")){
          context.getCounter(WORD_COUNTERS.MCAT).increment(1);
          while(itr.hasMoreTokens()) {
            context.getCounter(WORD_COUNTERS.MCAT_SUM).increment(1);
            WordOfD = l+"/"+itr.nextToken();
            word.set(WordOfD);
            context.write(word, one);
          }
        }
      }
    }
  }

  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }

  public static class UniqueMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      
      StringTokenizer labels = new StringTokenizer(value.toString(),"\t",false);
      labels.nextToken();
      StringTokenizer doc = new StringTokenizer(labels.nextToken());
      while(doc.hasMoreTokens()){
        word.set(doc.nextToken());
        context.write(word,one);
      }
    }
  }
  public static class UniqueReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      context.getCounter(WORD_COUNTERS.UNIQUE_WORDS).increment(1);
    }
  }
/*
  public static class TestMapper1
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      
    }
  }
  public static class TestReducer1
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      
    }
  }
*/
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job1 = Job.getInstance(conf, "training set word count");
    job1.setJarByClass(WordCount.class);
    job1.setMapperClass(TokenizerMapper.class);
    job1.setCombinerClass(IntSumReducer.class);
    job1.setReducerClass(IntSumReducer.class);
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job1, new Path(args[1]));
    job1.waitForCompletion(true);
   
    Job job2 = Job.getInstance(conf, "training set word count");
    job2.setJarByClass(WordCount.class);
    job2.setMapperClass(TokenizerMapper.class);
    job2.setCombinerClass(IntSumReducer.class);
    job2.setReducerClass(IntSumReducer.class);
    job2.setOutputKeyClass(Text.class);
    job2.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job2, new Path(args[0]));
    FileOutputFormat.setOutputPath(job2, new Path("tmp"));
    System.exit(job2.waitForCompletion(true) ? 0 : 1);

    Counters counters1 = job1.getCounters();
    Counters counters2 = job2.getCounters();
    Counter unique    = counters2.findCounter(WORD_COUNTERS.UNIQUE_WORDS);
    Counter all_train = counters1.findCounter(WORD_COUNTERS.ALL_TRAIN);
    Counter ccat      = counters1.findCounter(WORD_COUNTERS.CCAT);
    Counter ecat      = counters1.findCounter(WORD_COUNTERS.ECAT);
    Counter gcat      = counters1.findCounter(WORD_COUNTERS.GCAT);
    Counter mcat      = counters1.findCounter(WORD_COUNTERS.MCAT);
    Counter ccatsum      = counters1.findCounter(WORD_COUNTERS.CCAT_SUM);
    Counter ecatsum      = counters1.findCounter(WORD_COUNTERS.ECAT_SUM);
    Counter gcatsum      = counters1.findCounter(WORD_COUNTERS.GCAT_SUM);
    Counter mcatsum      = counters1.findCounter(WORD_COUNTERS.MCAT_SUM);
    
    conf.setLong("UNIQUE_WORDS",unique.getValue());
    conf.setLong("ALL_TRAIN",all_train.getValue()); 
    conf.setLong("CCAT",ccat.getValue());
    conf.setLong("CCAT_SUM",ccat.getValue());
    conf.setLong("ECAT",ecat.getValue());
    conf.setLong("ECAT_SUM",ecat.getValue());
    conf.setLong("GCAT",gcat.getValue());
    conf.setLong("GCAT_SUM",gcat.getValue());
    conf.setLong("MCAT",mcat.getValue());
    conf.setLong("MCAT_SUM",mcat.getValue());
   
  }
}
